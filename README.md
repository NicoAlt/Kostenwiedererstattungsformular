# Kostenwiedererstattungsformular

This is a tiny web app to fairly distribute expenses between persons.
A demo is hosted at [media.dorfbrunnen.eu](https://media.dorfbrunnen.eu/web/money/).

![screenshot showing form with 5 persons](screenshot.png)

## Licensing

This app is licensed under the GNU General Public License Version 3.
See [LICENSE.md](LICENSE.md) for more information.
