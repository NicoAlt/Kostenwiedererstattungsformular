/*
  Copyright (c) 2018-2020 Nico Alt
  SPDX-License-Identifier: GPL-3.0-only
  License-Filename: LICENSE.md
*/

var ELEMENTS = {
  "main": document.getElementById("main")
}

var TOTAL_EXPENSES = 0.00

var PERSONS = [ ]

var STRINGS
var STRINGS_LOADED = false
var MONEY_SYNONYM
var MONEY_SYNONYM_LOADED = false

load_cached_persons()
load_strings()
load_money_synonym()

function build_page() {
  if (!STRINGS_LOADED || !MONEY_SYNONYM_LOADED) {
    return
  }
  var main_element = ELEMENTS.main

  var main_header = document.createElement("h1")
  var main_header_text = get_string("title")
  main_header_text = String.format(main_header_text, MONEY_SYNONYM)
  main_header.innerText = main_header_text
  main_element.appendChild(main_header)
  ELEMENTS["main_header"] = main_header

  var persons_header = document.createElement("h2")
  persons_header.innerText = get_string("persons_title")
  main_element.appendChild(persons_header)
  ELEMENTS["persons_header"] = persons_header

  var persons_name_input = document.createElement("input")
  persons_name_input.placeholder = get_string("persons_name")
  main_element.appendChild(persons_name_input)
  ELEMENTS["persons_name_input"] = persons_name_input

  var persons_add_button = document.createElement("button")
  persons_add_button.innerText = get_string("persons_add")
  main_element.appendChild(persons_add_button)
  ELEMENTS["persons_add_button"] = persons_add_button

  var persons_list = document.createElement("ul")
  main_element.appendChild(persons_list)
  ELEMENTS["persons_list"] = persons_list

  var expenses_header = document.createElement("h2")
  expenses_header.innerText = get_string("expenses_title")
  main_element.appendChild(expenses_header)
  ELEMENTS["expenses_header"] = expenses_header

  var expenses_person_dropdown = document.createElement("select")
  main_element.appendChild(expenses_person_dropdown)
  ELEMENTS["expenses_person_dropdown"] = expenses_person_dropdown

  var expenses_amount_to_add = document.createElement("input")
  expenses_amount_to_add.placeholder = MONEY_SYNONYM
  main_element.appendChild(expenses_amount_to_add)
  ELEMENTS["expenses_amount_to_add"] = expenses_amount_to_add

  var expenses_add_button = document.createElement("button")
  expenses_add_button.innerText = get_string("expenses_add")
  main_element.appendChild(expenses_add_button)
  ELEMENTS["expenses_add_button"] = expenses_add_button

  var expenses_person_states = document.createElement("div")
  main_element.appendChild(expenses_person_states)
  ELEMENTS["expenses_person_states"] = expenses_person_states

  var refund_header = document.createElement("h2")
  var refund_header_text = get_string("refund_title")
  refund_header_text = String.format(refund_header_text, MONEY_SYNONYM)
  refund_header.innerText = refund_header_text
  main_element.appendChild(refund_header)
  ELEMENTS["refund_header"] = refund_header

  var refund_average = document.createElement("p")
  main_element.appendChild(refund_average)
  ELEMENTS["refund_average"] = refund_average

  var refund_suggestions = document.createElement("div")
  main_element.appendChild(refund_suggestions)
  ELEMENTS["refund_suggestions"] = refund_suggestions

  var reset_button = document.createElement("button")
  reset_button.innerText = get_string("reset")
  main_element.appendChild(reset_button)
  ELEMENTS["reset_button"] = reset_button

  add_event_listeners()
  render_persons()
}

function add_event_listeners() {
  var button_persons = ELEMENTS["persons_add_button"]
  var button_expenses = ELEMENTS["expenses_add_button"]
  var button_reset = ELEMENTS["reset_button"]

  var input_persons = ELEMENTS["persons_name_input"]
  var input_expenses = ELEMENTS["expenses_amount_to_add"]

  button_persons.addEventListener("click", function() {
    add_person()
  })
  button_expenses.addEventListener("click", function() {
    add_expense()
  })
  button_reset.addEventListener("click", function() {
    reset()
  })

  input_persons.addEventListener("keydown", function(event) {
    if (event.key === "Enter") {
      event.preventDefault();
      add_person()
    }
  })
  input_expenses.addEventListener("keydown", function(event) {
    if (event.key === "Enter") {
      event.preventDefault();
      add_expense()
    }
  })
}

function add_person() {
  var input_person = ELEMENTS["persons_name_input"]

  var person = {
    "name": input_person.value,
    "expenses": 0.00
  }
  PERSONS.push(person)
  write_persons_to_cache()

  input_person.value = ""
  input_person.focus()

  render_persons()
}

function add_expense() {
  var input_expenses = ELEMENTS["expenses_amount_to_add"]

  var person_index = ELEMENTS["expenses_person_dropdown"].value
  var expense = input_expenses.value
  expense = expense.replace(/,/g, '.')
  expense = parseFloat(expense)

  person = PERSONS[person_index]
  person.expenses = person.expenses + expense
  TOTAL_EXPENSES = TOTAL_EXPENSES + expense
  write_persons_to_cache()

  input_expenses.value = ""
  input_expenses.focus()

  render_persons()
}

function render_persons() {
  var persons_list = ELEMENTS["persons_list"]
  while (persons_list.firstChild) {
    persons_list.removeChild(persons_list.firstChild)
  }

  var expenses_person_dropdown = ELEMENTS["expenses_person_dropdown"]
  var selected_person_index = expenses_person_dropdown.selectedIndex
  for(i = expenses_person_dropdown.options.length - 1 ; i >= 0 ; i--) {
    expenses_person_dropdown.remove(i)
  }

  var expenses_person_states = ELEMENTS["expenses_person_states"]
  while (expenses_person_states.firstChild) {
    expenses_person_states.removeChild(expenses_person_states.firstChild)
  }

  for (i in PERSONS) {
    var person = PERSONS[i]
    var persons_list_item = document.createElement("li")
    persons_list_item.innerText = person.name
    persons_list.appendChild(persons_list_item)

    var expenses_person_dropdown_option = document.createElement("option")
    expenses_person_dropdown_option.value = i
    expenses_person_dropdown_option.innerText = person.name
    expenses_person_dropdown.appendChild(expenses_person_dropdown_option)

    var expenses_person_state = document.createElement("p")
    var expenses_person_state_text = get_string("expenses_person_state")
    expenses_person_state_text = String.format(expenses_person_state_text, person.name, person.expenses.toFixed(2), (person.expenses - (TOTAL_EXPENSES / PERSONS.length)).toFixed(2))
    expenses_person_state.innerText = expenses_person_state_text
    expenses_person_states.appendChild(expenses_person_state)
  }

  if (PERSONS.length != 0) {
    var refund_average = ELEMENTS["refund_average"]
    var refund_average_text = get_string("refund_average")
    refund_average_text = String.format(refund_average_text, MONEY_SYNONYM, (TOTAL_EXPENSES / PERSONS.length).toFixed(2))
    refund_average.innerText = refund_average_text
  }
  if (selected_person_index !== -1) {
    expenses_person_dropdown.selectedIndex = selected_person_index
  }
}

function get_string(name) {
  return STRINGS[name]
}

function load_strings() {
  var request = new XMLHttpRequest()
  request.open("GET", "assets/strings.json")
  request.onreadystatechange = function() {
    if (request.readyState === 4) {
      STRINGS = JSON.parse(request.response)
      STRINGS_LOADED = true
      build_page()
    }
  }
  request.send()
}

function load_money_synonym() {
  var request = new XMLHttpRequest()
  request.open("GET", "assets/money_synonyms.json")
  request.onreadystatechange = function() {
    if (request.readyState === 4) {
      var money_synonyms = JSON.parse(request.response)
      var random_synoym_index = Math.floor(Math.random() * money_synonyms.length)
      MONEY_SYNONYM = money_synonyms[random_synoym_index]
      MONEY_SYNONYM_LOADED = true
      build_page()
    }
  }
  request.send()
}

function load_cached_persons() {
  var persons_json = localStorage.getItem('persons')
  if (persons_json != null) {
    PERSONS = JSON.parse(persons_json)
  }
  calculate_total_expenses()
}

function write_persons_to_cache() {
  localStorage.setItem('persons', JSON.stringify(PERSONS))
}

function calculate_total_expenses() {
  for (i in PERSONS) {
    var person = PERSONS[i]
    TOTAL_EXPENSES = TOTAL_EXPENSES + person.expenses
  }
}

function reset() {
  localStorage.clear()
  location.reload()
}

// Java's String.format function in JavaScript
if (!String.format) {
  String.format = function(format) {
    var args = Array.prototype.slice.call(arguments, 1)
    return format.replace(/{(\d+)}/g, function(match, number) {
      return typeof args[number] != 'undefined' ? args[number] : match
    })
  }
}
